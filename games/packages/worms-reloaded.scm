;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Pierre Neidhardt <mail@ambrevar.xyz>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages worms-reloaded)
  #:use-module (ice-9 match)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (nonguix build-system binary)
  #:use-module (nonguix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages sdl)
  #:use-module (games humble-bundle))

(define-public worms-reloaded
  (let* ((version "1381858841")
         (file-name (string-append "WormsReloaded_Linux_" version ".sh"))
         (binary "WormsReloaded"))
    (package
      (name "worms-reloaded")
      (version version)
      (source
       (origin
         (method humble-bundle-fetch)
         (uri (humble-bundle-reference
               (help (humble-bundle-help-message name))
               (config-path (list (string->symbol name) 'key))
               (files (list file-name))))
         (file-name file-name)
         (sha256
          (base32
           "0pgdg3mq5dz9havv4sgpsz8w3cvld3rdd336pwm6vg3agvznl9iv"))))
      (build-system binary-build-system)
      (supported-systems '("i686-linux" "x86_64-linux"))
      (arguments
       `(#:system "i686-linux"
         #:patchelf-plan
         `(("data/x86/WormsReloaded.bin.x86"
            ("gcc" "openal" "mesa" "zlib" "sdl2")))
         #:install-plan
         `(("data/x86/" ("WormsReloaded.bin.x86" "WormsReloaded.png") "share/worms-reloaded/")
           ("data/noarch" (".") "share/worms-reloaded/"))
         #:phases
         (modify-phases %standard-phases
           (replace 'unpack
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke (which "makeself_safeextract")
                       "--mojo"
                       (assoc-ref inputs "source"))
               (chdir ((@@ (guix build gnu-build-system) first-subdirectory) "."))
               #t))
           (add-after 'install 'create-wrapper
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((output (assoc-ref outputs "out"))
                      (wrapper (string-append output "/bin/worms-reloaded"))
                      (real (string-append output "/share/worms-reloaded/WormsReloaded.bin.x86"))
                      (icon (string-append output "/share/worms-reloaded/WormsReloaded.png")))
                 (mkdir-p (dirname wrapper))
                 (symlink real wrapper)
                 (make-desktop-entry-file (string-append output "/share/applications/worms-reloaded.desktop")
                                          #:name "Worms Reloaded"
                                          #:exec wrapper
                                          #:icon icon
                                          #:comment "2D artillery turn-based tactics video game"
                                          #:categories '("Application" "Game")))
               #t)))))
      (native-inputs
       `(("makeself-safeextract" ,makeself-safeextract)))
      (inputs
       `(("gcc" ,gcc "lib")
         ("mesa" ,mesa)
         ("openal" ,openal)
         ("sdl2" ,sdl2)
         ("zlib" ,zlib)))
      (home-page "http://www.team17.com/")
      (synopsis "2D artillery turn-based tactics video game")
      (description "Worms Reloaded is a turn-based and returns to its original
2D gameplay for the first time on PC since Worms World Party in 2001.  There
are both single-player and multiplayer modes available with up to four players
online or local hotspot.

Players take turns controlling their team of worms with the ultimate goal of
taking out the opposing team(s).  The player must act within a time limit
before the turn ends.")
      (license (undistributable "No URL")))))
