;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019, 2020 Alex Griffin <a@ajgrf.com>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games gog-download)
  #:use-module (guix derivations)
  #:use-module (guix gexp)
  #:use-module (guix modules)
  #:use-module (guix monads)
  #:use-module (guix packages)
  #:use-module (guix profiles)
  #:use-module (guix store)
  #:use-module (games utils)
  #:use-module (ice-9 match)
  #:export (gog-fetch))

(define (lgogdownloader-package)
  "Return the default lgogdownloader package."
  (let ((module (resolve-interface '(nongnu packages gog))))
    (module-ref module 'lgogdownloader)))

(define (expect-package)
  "Return the default expect package."
  (let ((module (resolve-interface '(gnu packages tcl))))
    (module-ref module 'expect)))

(define (nss-certs-package)
  "Return the default nss-certs package."
  (let ((module (resolve-interface '(gnu packages certs))))
    (module-ref module 'nss-certs)))

(define gog-help-message
  (string-append
    "FETCH FAILED
============

Please ensure you have set the GOG.com email and password in\n`"
    (guix-gaming-channel-games-config)
    "`.

```scheme
(make-gaming-config
 '((gog ((email \"my-gog-email@example.com\")
         (password \"password\")))
   ...))
```

Alternatively, if you already have the game stored locally somewhere,
build the game with this additional option:
`--with-source=game=<path to the installer>`."))

(define* (gog-fetch url hash-algo hash
                    #:optional name
                    #:key (system (%current-system)) (guile (default-guile))
                    (lgogdownloader (lgogdownloader-package))
                    (nss-certs (nss-certs-package))
                    (expect (expect-package)))
  "Return a fixed-output derivation that fetches URL, which should use the
`gogdownloader://' protocol scheme as understood by lgogdownloader.  The output
is expected to HASH of type HASH-ALGO (a symbol).  Use NAME as the file name,
or a generic name if #f."
  (let* ((gog-email (read-config gog-help-message 'gog 'email))
         (gog-password (read-config gog-help-message 'gog 'password)))

    (define modules
      (source-module-closure '((guix build utils))))

    (define (build ca-certificates)
      (with-imported-modules modules
        #~(begin
            (use-modules (guix build utils))

            (let ((lgogdownloader (string-append #+lgogdownloader
                                                 "/bin/lgogdownloader"))
                  (expect (string-append #+expect "/bin/expect"))
                  (cert-file (string-append #+ca-certificates
                                            "/etc/ssl/certs/ca-certificates.crt"))
                  (wrapper "gog-download.exp"))
              ;; lgogdownloader insists on writing to these directories.
              (setenv "XDG_CONFIG_HOME" "config")
              (setenv "XDG_CACHE_HOME" "cache")

              ;; Point lgogdownloader to the certificate bundle.
              (setenv "CURL_CA_BUNDLE" cert-file)

              ;; Wrap lgogdownloader in an expect script to feed it credentials
              ;; non-interactively.
              (call-with-output-file wrapper
                (lambda (p)
                  (display (string-append "global env
set timeout -1
spawn -noecho " lgogdownloader " --output-file " #$output " $env(GOG_URL)
log_user 0
expect \"Email: \"
send -- \"$env(GOG_EMAIL)\\n\"
expect \"Password: \"
send -- \"$env(GOG_PASSWORD)\\n\"
log_user 1
expect {
    \"Login failed\" {exit 1}
    eof
}\n")
                           p)))
              (invoke expect "-f" wrapper)
              #t))))

    (mlet %store-monad
        ((guile (package->derivation guile system))
         (ca-certificates (ca-certificate-bundle
                           (packages->manifest (list nss-certs)))))
      (gexp->derivation (or name "gog-download") (build ca-certificates)

                        ;; Use environment variables and a fixed script name so
                        ;; there's only one script in store for all the
                        ;; downloads.
                        #:script-name "gog-download"
                        #:env-vars
                        `(("GOG_URL" . ,url)
                          ("GOG_EMAIL" . ,gog-email)
                          ("GOG_PASSWORD" . ,gog-password))
                        #:leaked-env-vars '("http_proxy" "https_proxy"
                                            "LC_ALL" "LC_MESSAGES" "LANG"
                                            "COLUMNS")
                        #:system system
                        #:local-build? #t
                        #:hash-algo hash-algo
                        #:hash hash
                        #:guile-for-build guile))))
