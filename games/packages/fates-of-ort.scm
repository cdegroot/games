;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2020 Timotej Lazar <timotej.lazar@araneo.si>
;;;
;;; This file is not part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (games packages fates-of-ort)
  #:use-module (games build-system mojo)
  #:use-module (games gog-download)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages xorg)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (nonguix licenses))

(define-public gog-fates-of-ort
  (let ((buildno "44274"))
    (package
      (name "gog-fates-of-ort")
      (version "1.2.2")
      (source
       (origin
         (method gog-fetch)
         (uri "gogdownloader://fates_of_ort/en3installer0")
         (file-name (string-append "fates_of_ort_"
                                   (string-replace-substring version "." "_")
                                   "_" buildno ".sh"))
         (sha256
          (base32 "1b4mjyilysdl3h8m1v2a5hj6dknhzcxxkqv6cpgxzhsm59jsm1mv"))))
      (supported-systems '("x86_64-linux"))
      (build-system mojo-build-system)
      (arguments
       `(#:patchelf-plan
         '(("FatesofOrt.x86_64"
            ("gcc:lib" "glu" "libstdc++" "libx11" "libxext" "libxi" "mesa" "openal")))
         #:phases
         (modify-phases %standard-phases
           (add-after 'install 'fix-libraries
             (lambda _
               ;; Let the game find libGalaxy64.so.
               (let* ((out (assoc-ref %outputs "out"))
                      (share (string-append out "/share/" ,name "-" ,version)))
                 (wrap-program (string-append share "/start.sh")
                   `("LD_LIBRARY_PATH" ":" prefix (,(string-append share "/game")))))
               #t)))))
      (inputs
       `(("gcc:lib" ,gcc "lib")
         ("glu" ,glu)
         ("libstdc++" ,(make-libstdc++ gcc))
         ("libx11" ,libx11)
         ("libxext" ,libxext)
         ("libxi" ,libxi)
         ("mesa" ,mesa)
         ("openal" ,openal)))
      (home-page "https://fatesofort.com")
      (synopsis "Retro fantasy role-playing game")
      (description "Fates of Ort is a retro inspired fantasy RPG with a focus
on strategic action and a story driven by your decisions.  It is a tale of
sacrifice and difficult choices, in a world under threat of annihilation by
the avaricious force of Consumption magic.

In Ort, time stands still when you do – but when you are moving or performing
an action, your enemies will come rushing to destroy you.  Use this ability to
mould time to your advantage by carefully avoiding projectiles and setting up
elaborate traps to defeat your enemies.")
      (license (undistributable
                (string-append "file://data/noarch/docs/"
                               "End User License Agreement.txt"))))))
